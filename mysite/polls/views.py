from django.shortcuts import redirect, render
from django.http import HttpResponse
from .forms import CreatePollForm
from .models import Poll 
from django.core.paginator import Paginator
from django.core.mail import send_mail

    
def create(request):
    if request.method == 'POST':
        form = CreatePollForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
    else:
        form = CreatePollForm()
    context = {
        'form': form
        }
    return render(request,'polls/create.html',context)

def vote(request,poll_id):  
    try: 
        poll = Poll.objects.get(pk=poll_id)
        if request.method == 'POST':
            selected_option = request.POST['poll']
            if selected_option == 'option1':
                poll.option_one_count += 1
            elif selected_option == 'option2':
                poll.option_two_count += 1
            elif selected_option == 'option3':
                poll.option_three_count += 1
            else:
                return HttpResponse(400, 'Invalid Form')
            poll.save()
            return redirect('results',poll_id)
    except Poll.DoesNotExist:
        poll = None
        
        
    context = {
        'poll' : poll
    }
    return render(request,'polls/vote.html',context)
    
def results(request,poll_id):
    try:
        poll = Poll.objects.get(pk = poll_id)
    except Poll.DoesNotExist:
        poll = None
        
    context = {
        'poll':poll
    }
    return render(request,'polls/results.html',context)



# filter record and show data in record.html 
def record(request):
    polls = Poll.objects.filter(question="s")
    context = {
        'polls':polls
    }

    return render(request,'polls/record.html',context)

def pagination(request):
    # # send mail code
    # send_mail(
    #     # subject for mail
    #     # Message of mail
    #     #Emailid 
    #     #user mail
    #     'Testing mail',
    #     'Welcome to the po    ll app',
    #     'pansuriyaronak1@gmail.com',
    #     ['ronak.pansuriya@drcsystems.in'],
    # )

    # **bulk_create()method
    # poll = [
    #     Poll(question ='what is?',option_one='A',option_two = 'B',option_three = 'C'),
    #     Poll(question ='what is?',option_one='A',option_two = 'B',option_three = 'C'),
    #     Poll(question ='what is?',option_one='A',option_two = 'B',option_three = 'C')
    # ]
    # data = Poll.objects.bulk_create(poll)

    # **bulk_update()method
    po = Poll.objects.all()
    for pol in po:
        pol.option_one = 'None'
    data = Poll.objects.bulk_update(po,['option_one'])

    # Delete data
    # pol = Poll.objects.get(pk=5).delete()    
    

    # PAGINATION
    polls = Poll.objects.all().order_by('id')
    paginator = Paginator(polls,4,orphans=2)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {
        'page_obj': page_obj,
        'data' : data,
        # 'pol':pol
    }
    return render(request,'polls/home.html',context)