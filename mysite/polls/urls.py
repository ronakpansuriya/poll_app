from django.urls import path
from . import views

urlpatterns = [
   path('create/',views.create, name='create'),
   path('results/<poll_id>/',views.results,name='results'),
   path('vote/<poll_id>/',views.vote,name='vote'),
   path('record',views.record,name='record'),
   path('',views.pagination,name='home'),
   ]
