from django.db import models
from django.contrib.auth.models import User


class Poll(models.Model):
    question = models.TextField()
    option_one = models.CharField(max_length=30,blank=True,null=True)
    option_two = models.CharField(max_length=30,null=True)
    option_three = models.CharField(max_length=30)
    option_one_count = models.IntegerField(default=0)
    option_two_count = models.IntegerField(default=0)
    option_three_count = models.IntegerField(default=0)

    def __str__(self):
        return self.question

    def total(self):
        return self.option_one_count + self.option_two_count + self.option_three_count

    class Meta:
        verbose_name = 'Poll Question'


class Choice(models.Model):
    choice = models.ForeignKey(Poll,on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=10)


# many to many relation ship
class blog(models.Model):
    user = models.ManyToManyField(User)
    blog_title = models.CharField(max_length=20)
    blog_dic = models.TextField()

    