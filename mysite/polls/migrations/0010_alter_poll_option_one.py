# Generated by Django 3.2.11 on 2022-01-20 05:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0009_alter_poll_option_one'),
    ]

    operations = [
        migrations.AlterField(
            model_name='poll',
            name='option_one',
            field=models.CharField(blank=True, max_length=30, null=True),
        ),
    ]
