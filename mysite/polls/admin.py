from django.contrib import admin
from .models import Poll,Choice,blog


admin.site.site_header = "Poll App"
# Register your models here.

class AdminPoll(admin.ModelAdmin):
    # display list
    list_display = ('question' , 'option_one','option_two','option_three')
    # show filter record
    list_filter = ('question',)
    # list_filter = ('option_one',)

    #search Filed
    search_fields = ['question']

    #editable_view
    editable_list = ['option_one']
    

@admin.register(blog)
class BlogAdmin(admin.ModelAdmin):
    list_display = ['blog_title','blog_dic']


admin.site.register(Poll,AdminPoll)
admin.site.register(Choice)