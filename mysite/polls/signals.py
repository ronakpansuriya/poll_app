from django.contrib.auth.signals import user_logged_in,user_logged_out,user_login_failed
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import pre_init,pre_save,pre_delete,post_delete,post_init,post_save

# using decorator
@receiver(user_logged_in, sender = User)
def login_success(sender,request,user, **kwargs):
    print("----------")
    print("login signals")
    print("sender",sender)
    print("user",user)
    print(kwargs)
    print("---------")
# manual connect route
# user_logged_in.connect(login_success,sender=User)

@receiver(user_logged_out, sender = User)
def log_out(sender,request,user, **kwargs):
    print("----------")
    print("logout signals")
    print("sender",sender)
    print("user",user)
    print(kwargs)
    print("---------")
# user_logged_out.connect(log_out,sender=User)

@receiver(user_login_failed, sender = User)
def login_faild(sender,request,credentials, **kwargs):
    print("----------")
    print("Auth faild signals")
    print("sender",sender)
    print("credentials",credentials)
    print(kwargs)
    print("---------")
# user_login_failed.connect(login_faild)


# model signals

# sent signals beginnig 
@receiver(pre_save,sender = User)
def at_beginnig(sender,instance, **kwags):
    print("----------")
    print("pre save signals")
    print("sender:", sender)
    print("instance:",instance)
    print("kwargs:",kwags)
# pre_save.connect(at_beginnig, sender = User)


# sent signal last

@receiver(post_save, sender = User)
def at_ending_save(sender,instance,created,**kwags):
    if created:
        print("--------")
        print("post save signals")
        print("New record")
        print("sender:", sender)
        print("instance:",instance)
        print("kwargs:",kwags)
    else:
        print("--------")
        print("post save signals")
        print("updated")
        print("sender:", sender)
        print("instance:",instance)
        print("kwargs:",kwags)

        



